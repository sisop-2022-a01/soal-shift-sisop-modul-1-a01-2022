#!/bin/bash

AGG_AWK="$HOME/aggregate.awk"
AGG_HOUR=$(date -d "1 hour ago" +%Y%m%d%H)
TARGET_PATH="$HOME"
LOG_PATH="${TARGET_PATH}/log"
LOGFILE="${LOG_PATH}/metrics_agg_${AGG_HOUR}.log"

# run $AGG_AWK script on metrics of last hour, specified by
# hour format $AGG_HOUR and any 4-letter after it
awk -F',' -f $AGG_AWK "${LOG_PATH}/metrics_${AGG_HOUR}"????".log" > $LOGFILE
chmod go-rwx $LOGFILE
