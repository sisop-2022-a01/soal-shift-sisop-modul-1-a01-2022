# Laporan Soal Shift Modul 1

Shell Scripting, Cron, dan AWK

Sistem Operasi Semester Genap 2022  
Institut Teknologi Sepuluh Nopember  
Kelompok A01

| ID  | Nama                        | NRP            |
| --- | --------------------------- | -------------- |
| MH  | Muhammad Hidayat            | 05111940000131 |
| FP  | Farrel Trianzah Putra       | 5025201065     |
| IP  | Immanuel Maruli Tua Pardede | 5025201166     |

Kami menggunakan standar berikut untuk penulisan:

- Username `USER` yang digunakan adalah `cactus`.

- Home directory `HOME` untuk `USER` adalah `/home/cactus`.

## Soal 1: Image Downloader with User Management

Dikerjakan oleh: IP

## Soal 2: Website Forensics

Dikerjakan oleh: FP

## Soal 3: Resource Monitoring

Dikerjakan oleh: MH

Untuk `<target_path>`, kami menggunakan variabel `$USER` yang berisi nama user yang sedang menjalankan script tersebut dan ditambahkan ke string `/home/` untuk merujuk ke _home directory_.

```bash
TARGET_PATH="/home/$USER"
```

File log yang dihasilkan akan ditaruh di 

```bash
LOG_PATH="$TARGET_PATH/log"
```

### Minute metrics

Format yang digunakan untuk logfile adalah `metrics_{YmdHMS}.log`. Tanggal dapat direpresentasikan dengan perintah `date +%Y%m%d%H%M%S`. Contoh hasil akhir nama file adalah `metrics_20220226101601.log`

```bash
DATE_OUT=$(date +%Y%m%d%H%M%S)
LOGFILE="${LOG_PATH}/metrics_${DATE_OUT}.log"
```

Sebelum memulai membuat log, terlebih dahulu pastikan bahwa folder log sudah ada. Jika belum, buat folder tersebut. Representasi pseudocode adalah "If folder `$LOG_PATH` does not exist, create folder `$LOG_PATH`". Berikut representasi dalam `bash`:

```bash
[ -d $LOG_PATH ] || mkdir $LOG_PATH
```

Langkah pertama adalah menampilkan penggunaan memori sistem. Perintah yang digunakan adalah `free -m`. Contoh hasil dari perintah tersebut adalah:

```
              total        used        free      shared  buff/cache   available
Mem:           3663        1647         333         285        1681        1447
Swap:          2047        1207         840
```

Kami menggunakan `awk` untuk memproses keluaran tersebut. Pertama, kami mengubah output field separator (`OFS`) menjadi koma `,`. Input yang diproses hanya dari baris kedua dan dari kolom kedua. Kolom pertama dapat dibuang dengan membuat kolom pertama menjadi string kosong `""`. Untuk membuang koma berlebih di awal, kami menggunakan fungsi substitusi string `sub` yang memanfaatkan _regular expression_ (regex). Kami juga menambahkan koma di akhir output. Terakhir, agar semua output berada dalam satu baris, kami mengganti `print` dengan `printf` karena `print` membuat baris baru secara otomatis tiap entri input baris sedangkan `printf` tidak.

Perintah `awk` berdasarkan deskripsi di atas adalah

```awk
BEGIN { OFS = "," }
NR>1 { 
    $1 = "";
    sub(/,/, "");
    printf $0",";
}
```

Keluaran dari `free -m` setelah dimanipulasi dengan `awk` menjadi

```
3663,1647,333,285,1681,1447,2047,1207,840,
```

Langkah kedua adalah membaca penggunaan disk (_disk usage_ (`du`)). Perintah yang digunakan adalah `du -sh $TARGET_PATH` dan menghasilkan _output_ berikut

```
14G /home/cactus
```

Karena hanya terdapat satu baris dan dua field, maka manipulasi `awk` sangat sederhana, yaitu field kedua (direktori) dan field pertama (ukuran), dipisahkan oleh koma. Perintah `awk` yang akan dijalankan adalah 

```awk
{print $2","$1}
```

dan setelah dimanipulasi menjadi

```
/home/cactus,14G
```

Langkah ketiga adalah membuat output ke log file `$LOGFILE`. File terdiri dari dua baris: nama kolom dan nilai. Nama kolom telah ditentukan, sedangkan nilai merupakan gabungan dari keluaran `free` dan `du` yang masing-masing telah dimanipulasi melalui `awk`. Contoh hasil keluaran file adalah dalam format _comma-separated values_ (CSV)

```csv
mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
3663,1658,302,306,1702,1415,2047,1207,840,/home/cactus,14G
```

Langkah terakhir untuk keamanan adalah mengatur izin file log agar hanya bisa dibaca oleh user pemilik file itu sendiri. User lain tidak berhak membaca file-file tersebut. Perintah yang digunakan adalah `chmod go-rwx $LOGFILE`, yaitu mencabut seluruh akses untuk selain pemilik file tersebut, yaitu user itu sendiri.

Script dijalankan dengan `./minute_log.sh` dengan hasil yang diharapkan, baik isi output maupun lokasi dan perizinan file output. Berikut contoh hasil output dari `ls -l ~/log`

```
-rw------- 1 cactus cactus 167 Feb 26 11:19 metrics_20220226111901.log
-rw------- 1 cactus cactus 167 Feb 26 11:20 metrics_20220226112001.log
-rw------- 1 cactus cactus 167 Feb 26 11:21 metrics_20220226112101.log
```

### Schedule minute metrics

Script `minute_log.sh` akan dijalankan tiap menit oleh user itu sendiri menggunakan `cron`. Misalkan `minute_log.sh` ditaruh dalam folder `~/.local/bin`, isi `crontab` adalah sebagai berikut:

```crontab
* * * * * ./.local/bin/minute_log.sh
```

Setelah `crontab` diatur untuk menjalankan script `minute_log.sh` tiap menit, tidak ada file apapun yang muncul secara otomatis di folder `~/log`. Saat memeriksa log untuk `cron` menggunakan perintah `systemctl status cron.service`, berikut isi yang kami terima:

```
● cron.service - Regular background program processing daemon
     Loaded: loaded (/lib/systemd/system/cron.service; enabled; vendor preset: enabled)
     Active: active (running) since Fri 2022-02-25 17:15:25 WIB; 16h ago
       Docs: man:cron(8)
   Main PID: 663 (cron)
      Tasks: 6 (limit: 4267)
     Memory: 576.6M
     CGroup: /system.slice/cron.service
             ├─   663 /usr/sbin/cron -f
             ├─128975 /usr/sbin/CRON -f
             ├─128976 /bin/bash /home/cactus/.local/bin/minute_log.sh
             ├─128982 /bin/bash /home/cactus/.local/bin/minute_log.sh
             ├─128983 du -sh /home/
             └─128984 awk -F\t {print $2","$1}
```

Permasalahan timbul karena `cron` melalui `bash` tidak mampu membaca isi variabel `$USER`, sehingga dianggap kosong. Selanjutnya, script mencoba membuat file log di `/home//log` namun tidak berhasil karena larangan izin dan folder tersebut tidak ada. Oleh karena itu, folder yang diharapkan, yaitu `/home/cactus/log` tidak pernah terisi.

Lain halnya ketika `$TARGET_PATH` diganti menjadi

```bash
TARGET_PATH=$HOME
```

script berjalan dengan normal dan sesuai ekspektasi. Folder `log` terisi dengan rekaman resource tiap menitnya.

### Hourly summary

Selanjutnya adalah agregasi file log tiap menit ke satuan jam. Misalkan semua metrik pada `2022-02-26` pada jam ke 10, semua file yang muncul dalam glob `metrics_2022022610????.log` akan dibaca dan dibuat analisis dari semua file tersebut. `?` melambangkan tepat satu karakter harus muncul dalam nama tersebut. Script akan dijalankan tiap jam dan akan melakukan agregasi terhadap data pada jam sebelumnya. Waktu pada jam sebelumnya ditentukan dengan perintah

```bash
AGG_HOUR=$(date -d "1 hour ago" +%Y%m%d%H)
```

Analisis dan manipulasi dilakukan menggunakan `awk`. Untuk mendapatkan nilai `max`, `min`, dan `avg` dari tiap entri, dilakukan perhitungan manual. Karena tiap file memiliki dua baris, hanya baris kedua dari tiap file yang akan dibaca dan dihitung banyaknya ke dalam variabel `count`. Untuk tiap kolom, baca nilai maksimum `max`, minimum `min`, dan total `sum` nilai dan masukkan ke dalam array sesuai indeks kolom. Khusus untuk kolom ke-10 (`path`), cukup catat nama `path` ke dalam variabel sekali saja. Untuk rata-rata `avg`, `sum` dari tiap kolom dibagi dengan `count`.

Terakhir, cetak semua informasi sesuai format yang ditentukan. Untuk `max`, `min`, dan `avg`, output dengan koma antar kolom. Kolom pertama adalah tipe agregasi, yaitu `maximum`, `minimum`, dan `average` berturut-turut. Kolom kedua dan seterusnya adalah isi dari ketiga array agregasi tersebut, dimana kolom ke-10 (`path`) dari array digantikan dengan variabel `path`.

Output diarahkan ke dalam file `metrics_agg_${AGG_HOUR}.log` dan diberikan izin sesuai ketentuan sebelumnya. Contoh hasil manipulasi `awk` adalah sebagai berikut:

```csv
type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
maximum,3663,2264,522,358,1679,1335,2047,1936,437,/home/cactus,14G
minimum,3663,1741,91,265,1253,778,2047,1610,111,/home/cactus,14
average,3663,1942.28,195.267,311.333,1524.65,1126.78,2047,1753.85,293.15,/home/cactus,14
```

Script `aggregate.awk` nantinya ditaruh di `$HOME` dengan izin yang lebih ringan daripada file log, yaitu dapat dibaca oleh semuanya, namun hanya dapat diubah oleh user pemilik file.

Kalimat `cron` untuk agregasi per jam adalah

```crontab
0 * * * * ./.local/bin/aggregate_minutes_to_hourly_log.sh
```