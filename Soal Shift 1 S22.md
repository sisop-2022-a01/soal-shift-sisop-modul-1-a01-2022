# Soal Shift Modul 1 Sistem Operasi 2022

## Peraturan

1. Waktu pengerjaan dimulai selasa(22/2) pukul 10.00 WIB hingga sabtu(26/2) pukul 22.00 WIB.

2. Praktikan diharapkan membuat laporan penjelasan dan penyelesaian soal dalam bentuk Readme(gitlab).

3. Format nama repository gitlab `soal-shift-sisop-modul-Y-XXX-2022` (contoh:`soal-shift-sisop-modul-1-A01-2022`).

4. Struktur repository seperti berikut:
   
   - readme.md
   - soal1
     - register.sh
     - main.sh
   - soal2
     - soal2_forensic_dapos.sh
   - soal3
     - minute_log.sh
     - aggregate_minutes_to_hourly_log.sh
   
   Jika melanggar struktur repo akan dianggap sama dengan curang dan menerima konsekuensi sama dengan melakukan kecurangan.

5. Setelah pengerjaan selesai, semua script `bash`, `awk`, dan file yang berisi cron job ditaruh di gitlab masing - masing kelompok, dan link gitlab diletakkan pada form yang disediakan.

6. Commit terakhir maksimal 10 menit setelah waktu pengerjaan berakhir. Jika melewati maka akan dinilai berdasarkan commit terakhir.

7. Jika tidak ada pengumuman perubahan soal oleh asisten, maka soal dianggap dapat diselesaikan.

8. Jika ditemukan soal yang tidak dapat diselesaikan, harap menuliskannya pada Readme beserta permasalahan yang ditemukan.

9. Praktikan tidak diperbolehkan menanyakan jawaban dari soal yang diberikan kepada asisten maupun praktikan dari kelompok lainnya.

10. Jika ditemukan **indikasi kecurangan** dalam bentuk apapun di pengerjaan soal shift, maka **nilai dianggap 0**.

11. Pengerjaan soal shift sesuai dengan modul yang telah diajarkan.

12. Zip dari repository dikirim ke email asisten penguji dengan subjek yang sama dengan nama judul repository, dikirim sebelum deadline dari soal shift

## Soal

1. Pada suatu hari, Han dan teman-temannya diberikan tugas untuk mencari foto. Namun, karena laptop teman-temannya rusak ternyata tidak bisa dipakai karena rusak, Han dengan senang hati memperbolehkan teman-temannya untuk meminjam laptopnya. Untuk mempermudah pekerjaan mereka, Han membuat sebuah program.
   
   1. Han membuat sistem register pada script `register.sh` dan setiap user yang berhasil didaftarkan disimpan di dalam file `./users/user.txt`. Han juga membuat sistem login yang dibuat di script `main.sh`
   2. Demi menjaga keamanan, input password pada login dan register harus tertutup/hidden dan password yang didaftarkan memiliki kriteria sebagai berikut
      - Minimal 8 karakter
      - Memiliki minimal 1 huruf kapital dan 1 huruf kecil
      - Alphanumeric
      - Tidak boleh sama dengan username
   3. Setiap percobaan login dan register akan tercatat pada log.txt dengan format : `MM/DD/YY hh:mm:ss MESSAGE`. Message pada log akan berbeda tergantung aksi yang dilakukan user.
      - Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah `REGISTER: ERROR User already exists`
      - Ketika percobaan register berhasil, maka message pada log adalah `REGISTER: INFO User USERNAME registered successfully`
      - Ketika user mencoba login namun passwordnya salah, maka message pada log adalah `LOGIN: ERROR Failed login attempt on user USERNAME`
      - Ketika user berhasil login, maka message pada log adalah `LOGIN: INFO User USERNAME logged in`
   4. Setelah login, user dapat mengetikkan 2 command dengan dokumentasi sebagai berikut :
      - `dl N` (`N` = Jumlah gambar yang akan didownload)
        Untuk mendownload gambar dari `https://loremflickr.com/320/240` dengan jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan dimasukkan ke dalam folder dengan format nama `YYYY-MM-DD_USERNAME`. Gambar-gambar yang didownload juga memiliki format nama `PIC_XX`, dengan nomor yang berurutan (contoh : `PIC_01`, `PIC_02`, dst. ).  Setelah berhasil didownload semua, folder akan otomatis di zip dengan format nama yang sama dengan folder dan dipassword sesuai dengan password user tersebut. Apabila sudah terdapat file zip dengan nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu, barulah mulai ditambahkan gambar yang baru, kemudian folder di zip kembali dengan password sesuai dengan user.
      - `att`
        Menghitung jumlah percobaan login baik yang berhasil maupun tidak dari user yang sedang login saat ini.

2. Pada tanggal 22 Januari 2022, website `https://daffa.info` di hack oleh seseorang yang tidak bertanggung jawab. Sehingga hari sabtu yang seharusnya hari libur menjadi berantakan. Dapos langsung membuka log website dan menemukan banyak request yang berbahaya. Bantulah Dapos untuk membaca log website `https://daffa.info`. Buatlah sebuah script awk bernama `soal2_forensic_dapos.sh` untuk melaksanakan tugas-tugas berikut:
   
   1. Buat folder terlebih dahulu bernama `forensic_log_website_daffainfo_log`.
   2. Dikarenakan serangan yang diluncurkan ke website https://daffa.info sangat banyak, Dapos ingin tahu berapa rata-rata request per jam yang dikirimkan penyerang ke website. Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama ratarata.txt ke dalam folder yang sudah dibuat sebelumnya.
   3. Sepertinya penyerang ini menggunakan banyak IP saat melakukan serangan ke website https://daffa.info, Dapos ingin menampilkan IP yang paling banyak melakukan request ke server dan tampilkan berapa banyak request yang dikirimkan dengan IP tersebut. Masukkan outputnya kedalam file baru bernama result.txt kedalam folder yang sudah dibuat sebelumnya.
   4. Beberapa request ada yang menggunakan user-agent ada yang tidak. Dari banyaknya request, berapa banyak requests yang menggunakan user-agent curl?
      Kemudian masukkan berapa banyak requestnya kedalam file bernama result.txt yang telah dibuat sebelumnya.
   5. Pada jam 2 pagi pada tanggal 23 terdapat serangan pada website, Dapos ingin mencari tahu daftar IP yang mengakses website pada jam tersebut. Kemudian masukkan daftar IP tersebut kedalam file bernama result.txt yang telah dibuat sebelumnya.
   
   Agar Dapos tidak bingung saat membaca hasilnya, formatnya akan dibuat seperti ini:
   
   - File ratarata.txt
     
         Rata-rata serangan adalah sebanyak rata_rata requests per jam
   
   - File result.txt
     
         IP yang paling banyak mengakses server adalah: ip_address sebanyak jumlah_request requests
         
         Ada jumlah_req_curl requests yang menggunakan curl sebagai user-agent
         
         IP Address Jam 2 pagi
         IP Address Jam 2 pagi
         dst
   
   footnote
   
   1. Gunakan AWK
   2. Nanti semua file-file HASIL SAJA yang akan dimasukkan ke dalam folder forensic_log_website_daffainfo_log

3. Ubay sangat suka dengan komputernya. Suatu saat komputernya crash secara tiba-tiba :(. Tentu saja Ubay menggunakan linux. Akhirnya Ubay pergi ke tukang servis untuk memperbaiki laptopnya. Setelah selesai servis, ternyata biaya servis sangatlah mahal sehingga ia harus menggunakan dana kenakalannya untuk membayar biaya servis tersebut. Menurut Mas Tukang Servis, laptop Ubay overload sehingga mengakibatkan crash pada laptopnya. Karena tidak ingin hal serupa terulang, Ubay meminta kalian untuk membuat suatu program monitoring resource yang tersedia pada komputer.
   
   Buatlah program monitoring resource pada komputer kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk `target_path` yang akan dimonitor adalah `/home/{user}/`.
   
   1. Masukkan semua metrics ke dalam suatu file log bernama `metrics_{YmdHms}.log`.` {YmdHms}` adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada `2022-01-31 15:00:00`, maka file log yang akan tergenerate adalah `metrics_20220131150000.log`.
   2. Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.
   3. Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi `metrics_agg_2022013115.log` dengan format `metrics_agg_{YmdH}.log`
   4. Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.
   
   Note:
   
   - nama file untuk script per menit adalah `minute_log.sh`
   - nama file untuk script agregasi per jam adalah `aggregate_minutes_to_hourly_log.sh`
   - semua file log terletak di `/home/{user}/log`
   
   Berikut adalah contoh isi dari file metrics yang dijalankan tiap menit:
   
       mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
       15949,10067,308,588,5573,4974,2047,43,2004,/home/youruser/test/,74M
   
   Berikut adalah contoh isi dari file aggregasi yang dijalankan tiap jam:
   
       type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
       minimum,15949,10067,223,588,5339,4626,2047,43,1995,/home/user/test/,50M
       maximum,15949,10387,308,622,5573,4974,2047,52,2004,/home/user/test/,74M
       average,15949,10227,265.5,605,5456,4800,2047,47.5,1999.5,/home/user/test/,62
