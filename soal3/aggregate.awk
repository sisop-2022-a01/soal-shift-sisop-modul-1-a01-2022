BEGIN {
    # init global vars
    count=0;
    alength=11;
}

function printMax() {
    for (i=0; i<=alength; ++i) {
        switch (i) {
        case 0:
            printf "maximum";
            break;
        case 10:
            printf "%s", path;
            break;
        default:
            printf "%s", max[i];
        }

        if (i!=alength) {
            printf ",";
        }
    }
    printf "\n";
}

function printMin() {
    for (i=0; i<=alength; ++i) {
        switch (i) {
        case 0:
            printf "minimum";
            break;
        case 10:
            printf "%s", path;
            break;
        default:
            printf "%s", min[i];
        }

        if (i!=alength) {
            printf ",";
        }
    }
    printf "\n";
}

function printAvg() {
    # create avg
    for (i=1; i<=alength; ++i) {
        if (i==10) {
            continue;
        };
        avg[i] = sum[i]/count;
    }

    for (i=0; i<=alength; ++i) {
        switch (i) {
        case 0:
            printf "average";
            break;
        case 10:
            printf "%s", path;
            break;
        default:
            printf "%s", avg[i];
        }

        if (i!=alength) {
            printf ",";
        }
    }
    printf "\n";
}

NR%2==0 {
    if (NR == 2) {
        for (i=1; i<=NF; ++i) {
            if (i==10) {
                path=$i;
                continue;
            };
            # to force values as number
            max[i] = $i+0;
            min[i] = $i+0;
            sum[i] = $i+0;
        }
        count  = 1;
    } else {
        for (i=1; i<=NF; ++i) {
            if (i==10) {
                continue;
            }
            if ($i > max[i]) max[i] = $i;
            if ($i < min[i]) min[i] = $i;
            sum[i] += $i;
        }
        count++;
    }
}

END {
    print "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size";
    printMin();
    printMax();
    printAvg();
}
