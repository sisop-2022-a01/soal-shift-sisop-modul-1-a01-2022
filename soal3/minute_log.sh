#!/bin/bash

#TARGET_PATH="/home/${USER}"
TARGET_PATH="$HOME"
LOG_PATH="${TARGET_PATH}/log"
DATE_OUT=$(date +%Y%m%d%H%M%S)
LOGFILE="${LOG_PATH}/metrics_${DATE_OUT}.log"

[ -d $LOG_PATH ] || mkdir $LOG_PATH

free_awk='
BEGIN { OFS = "," }
NR>1 { 
    $1 = "";
    sub(/,/, "");
    printf $0",";
}
'

free_res=$(free -m | awk "$free_awk")

du_awk='{print $2","$1}'
du_res=$(du -sh $TARGET_PATH 2> /dev/null | awk -F'\t' "$du_awk")

echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
${free_res}${du_res}" > $LOGFILE
chmod go-rwx $LOGFILE

